
import os
import subprocess
import tempfile
import logging
import conf
import base64
import json
from winrm import Session
from winrm import Protocol

from flask import Flask, request

app = Flask(__name__)

@app.route("/ntlm", methods=["GET"])
def ntlmConnection():
    return (test('ntlm'), 204)

@app.route("/basic", methods=["GET"])
def basicConnection():
    return (test('basic'), 204)

@app.route("/ssl", methods=["GET"])
def sslConnection():
    return (test('ssl'), 204)

def go(transport):
    session = Session(conf.HOST, auth=(conf.USER, conf.PASSWORD), transport=transport)
    result = session.run_ps('hostname')
    return result

def test(transport): 
    p = Protocol(
        endpoint=f'https://{conf.HOST}:5986/wsman',
        transport=transport,
        username=conf.USER,
        password=conf.PASSWORD,
        server_cert_validation='ignore')
    shell_id = p.open_shell()
    command_id = p.run_command(shell_id, 'dir')
    std_out, std_err, status_code = p.get_command_output(shell_id, command_id)
    p.cleanup_command(shell_id, command_id)
    p.close_shell(shell_id)
    return std_out

if __name__ == '__main__':
    PORT = int(os.getenv("PORT")) if os.getenv("PORT") else 8080
    # Rodar localmente, para verificar como rodar no conteiner verifique o Dockerfile
    app.run(host="127.0.0.1", port=PORT, debug=True)

