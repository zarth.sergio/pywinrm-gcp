from os import environ

HOST = environ.get('HOST')
USER = environ.get('USUARIO')
PASSWORD = environ.get('PASSWORD')
