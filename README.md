# Conexão GCP + windows (imagem base gcp) + powershell via python (pywinrm)

Projeto flask de teste de conexão para rodar comandos powershell com vm's windows criadas a partir de imagens base da gcp utilizando pywinrm
O código possui algumas rotas e duas funções que servem para realizar os testes de protocolo e as diferentes maneiras de conexão, basta ir alterando as chamadas
## Requisitos

* Resetar a senha do usuário na vm
   ```bash
   gcloud beta compute --project "${PROJECT_ID}" reset-windows-password "${INSTANCE_ID}" --zone "${ZONE}"
   ```

* Regra de firewall liberando a porta ```5986``` na vm

## Particularidades

Devido a algumas questões específicas de segurança e design do windows, o schema de acesso (com o pywinrm + gcp) obrigatoriamente deve ser o HTTPS utilizando o transport ```ntml```, porém, na maioria das vezes que uma máquina é criada o certificado não é válido, o que acarreta problemas utilizando o código normal de conexão:

```python
session = winrm.Session(conf.HOST, auth=(conf.USER, conf.PASSWORD), transport=transport)
```

Para contornar isso sem precisarmos entrar na máquina e liberar firewall e/ou darmos permissão para conexão ```basic``` (o que pode acarretar problemas de segurança), criamos um protocolo de comunicação na mão e indicamos para ignorar a verificação de certificado.

