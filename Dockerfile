FROM gcr.io/google.com/cloudsdktool/cloud-sdk

RUN apt-get update && apt-get install -y \
    python3-pip

COPY requirements.txt ./

RUN pip3 install -r requirements.txt
ENV PORT=8080
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./

CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --timeout 0 main:app